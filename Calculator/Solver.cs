﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Calculator
{
    public static class Solver
    {



        public static double Calculate(double numberOne, double numberTwo, string symbol)
        {
            double result = 0;
            switch (symbol)
            {
                case "÷":
                    result = numberOne / numberTwo;
                    break;
                case "*":
                    result = numberOne * numberTwo;
                    break;
                case "+":
                    result = numberOne + numberTwo;
                    break;
                case "-":
                    result = numberOne - numberTwo;
                    break;


            }
            return result;

        }


    }

}